# include <stdio.h>
int main()
{
    int x, y, sum, diff, prod, rem;
    float quo;
    printf("Enter any 2 Integers\n");
    scanf("%d%d", &x, &y);
    sum = x + y;
    diff = x - y;
    prod = x * y;
    quo = (float)x / y;
    rem = x % y;
    printf("Sum = %d\n", sum);
    printf("Difference = %d\n", diff);
    printf("Product = %d\n", prod);
    printf("Quotient = %f\n", quo);
    printf("Remainder = %d\n", rem);
    return 0;
}