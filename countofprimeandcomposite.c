# include <stdio.h>
int main()
{
    int n, cp=0, cc=0, f=0, i;
    printf("Enter any integer or 0 to end the program\n");
    scanf("%d", &n);
    while(n!=0)
    {
        for(i=2; i<=n/2; i++)
        {
            if(n%i==0)
            {
                f=1;
                break;
            }
        }
        if(f==1)
        {
            cc++;
        }
        else
        {
            cp++;
        }
        printf("Enter any integer or 0 to end the program\n");
        scanf("%d", &n);
        f=0;
    }
    printf("The total number of prime numbers is %d\n", cp);
    printf("The total number of composite numbers is %d\n", cc);
    return 0;
}