# include <stdio.h>
int main()
{
    int marks[3][5];
    int i=0, j=0, large=-1;
    printf("Enter the marks of 5 students in 3 subjects\n");
    for(i=0; i<3; i++)
    {
        for(j=0; j<5; j++)
        {
            scanf("%d", &marks[i][j]);
        }
        printf("\n");
    }
    for(i=0; i<3; i++)
    {
        for(j=0; j<5; j++)
        {
            printf("%d", &marks[i][j]);
        }
        printf("\n");
    }
    for(i=0; i<3; i++)
    {
        for(j=0; j<5; j++)
        {
            if(large<marks[i][j])
            {
                large=marks[i][j];
            }
        }
        printf("The highest marks in subject %d is %d\n", i, large);
    }
}