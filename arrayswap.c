# include <stdio.h>
int main()
{
    int i, n, a[20], small=999, large=-999, posS, posL, temp;
    printf("Enter the value of n\n");
    scanf("%d", &n);
    printf("Enter the array elements\n");
    for(i=0; i<n; i++)
    {
        scanf("%d", &a[i]);
        posS=0;
        posL=0;
    }
    for(i=0; i<n; i++)
    {
        if(a[i]<small)
        {
            small=a[i];
            posS=i;
        }
        if(a[i]>large)
        {
            large=a[i];
            posL=i;
        }
    }
    printf("The smallest and the largest number is %d and %d\n", small, large);
    temp=a[posL];
    a[posL]=a[posS];
    a[posS]=temp;
    printf("The new array is\n");
    for(i=0; i<n; i++)
    {
        printf("%d", a[i]);
    }
    return 0;
}