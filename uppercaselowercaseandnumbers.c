# include <stdio.h>
int main()
{
    int x=0, y=0, z=0;
    char c;
    printf("Enter a character or * to end the program\n");
    scanf(" %c", &c);
    while(c!='*')
    {
        if(c>='A' && c<='Z')
        {
            x++;
        }
        else if (c>='a' && c<='z')
        {
            y++;
        }
        else if(c>='0' && c<='9')
        {
            z++;
        }
        printf("Enter a character or * to end the program\n");
        scanf(" %c", &c);
    }
    printf("The number of uppercase character are %d\n", x);
    printf("The number of lowercase character are %d\n", y);
    printf("The number of number character are %d\n", z);
    return 0;
}
