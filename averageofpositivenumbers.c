# include <stdio.h>
int main ()
{
    int i, sum=0, cp=0, cn=0;
    printf("Enter any integer or 0 to end the program\n");
    scanf("%d", &i);
    while(i!=0)
    {
        if(i>0)
        {
            sum=sum+i;
            cp++;
        }
        else
        {
            cn++;
        }
        printf("Enter any integer or 0 to end the program\n");
        scanf("%d", &i);
    }
    printf("The total number of the positive numbers is %d\n", cp);
    printf("The total number of the negative numbers is %d\n", cn);
    printf("The average of the positive numbers is %f\n", (float)sum/cp);
    return 0;
}