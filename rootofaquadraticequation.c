#include<stdio.h>
#include<math.h>
int main()
{
	float a,b,c,r;
	printf("Enter the co-efficients of x, y& z\n");
	scanf("%f%f%f",&a,&b,&c);
	r=b*b-4*a*c;
	if(r>0)
	{
		printf("The roots are real and distinct.\nRoot1=%f\nRoot2=%f", (-b+sqrt(r))/2*a, (-b-sqrt(r))/2*a);
	}
	else if(r==0)
	{
		printf("The roots are real and equal.\nRoot1=%f\nRoot2=%f", -b/2*a, -b/2*a);
	}
	else
	{
		printf("The roots are imaginary.\n Root1=%f\nRoot2=%f", -b+sqrt(fabs(r))/(2*a), -b-sqrt(fabs(r))/(2*a));
	}
	return 0;
}	