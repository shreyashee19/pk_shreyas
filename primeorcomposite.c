# include <stdio.h>
int main()
{
    int n, i, f;
    printf("Enter any integer\n");
    scanf("%d", &n);
    if(n==1)
    {
        printf("The given integer is neither prime nor composite\n");
    }
    else
    {
        for(i=2; i<n; i++)
        {
            if(n%i==0)
                {
                    f=1;
                    break;
                }
        }
        if(f==1)
        {
            printf("The given integer is a composite number\n");
        }
        else
        {
            printf("The given integer is a prime number\n");
        }
    }
    return 0;
}