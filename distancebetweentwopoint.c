# include <stdio.h>
# include <math.h>
int main()
{
    float x1, x2, y1, y2, d;
    printf("Enter the Points\n");
    scanf("%f%f%f%f", &x1, &x2, &y1, &y2);
    d = sqrt(pow((x2-x1), 2)+pow((y2-y1), 2));
    printf("Distance between 2 points = %f", d);
    return 0;
}