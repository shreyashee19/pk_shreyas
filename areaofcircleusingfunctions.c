# include <stdio.h>
float area(float r)
{
    float a;
    a = 3.14 * r * r;
    return a;
}
int main()
{
    float r;
    printf("Enter the radius\n");
    scanf("%f", &r);
    printf("Area of Circle = %f", area(r));
    return 0;
}