# include <stdio.h>
int main()
{
    float r, area;
    printf("Enter the Radius\n");
    scanf("%f", &r);
    area = 3.14 * r * r;
    printf("Area of Circle = %f", area);
    return 0;
}